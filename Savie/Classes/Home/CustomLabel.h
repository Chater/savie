//
//  CustomLabel.h
//  Savie
//
//  Created by Chater on 7/6/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomLabel : UILabel

+ (instancetype)labelWithFrame:(CGRect)frame size:(CGFloat)size;

@end
