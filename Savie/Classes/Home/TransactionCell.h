//
//  TransactionCell.h
//  Savie
//
//  Created by Chater on 25.06.14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *transactionIcon;
@property (nonatomic, weak) IBOutlet UILabel *transactionTitle;
@property (nonatomic, weak) IBOutlet UILabel *transactionAmount;
@property (nonatomic, weak) IBOutlet UIImageView *separatorView;

- (void)updateCellWithTransaction:(Transaction *)transaction;

@end
