//
//  HeaderView.h
//  Savie
//
//  Created by Chater on 6/29/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderView : UIView

@property (nonatomic, strong) UIView *amountView;
@property (nonatomic, strong) UIView *calendarView;

- (void)animateForScrollOffset:(CGFloat)scrollOffset;

@end
