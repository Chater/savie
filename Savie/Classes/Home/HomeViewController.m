//
//  MainViewController.m
//  Savie
//
//  Created by Chater on 24.06.14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "HomeViewController.h"
#import "REFrostedViewController.h"
#import "TransactionCell.h"
#import "HeaderView.h"
#import "TableHeaderView.h"
#import "NavigationController.h"

#import "ModalTransitioningDelegate.h"

#import "UIImage+Tint.h"

@interface HomeViewController ()

@property (nonatomic, weak) IBOutlet HeaderView *headerView;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *top;

@property (nonatomic, assign) BOOL firstLaunch;

@property (nonatomic, strong) ModalTransitioningDelegate *transitioningDelegate;

@property (nonatomic, strong) NSArray *transactions;

@end

@implementation HomeViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"slide_menu_icon"]
                                                                           style:UIBarButtonItemStyleBordered
                                                                          target:self
                                                                          action:@selector(_showMenu)];
  
  self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"add_icon"]
                                                                           style:UIBarButtonItemStyleBordered
                                                                          target:self
                                                                          action:@selector(_addNewTransaction)];
  
  self.tableView.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0);
  self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(-64, 0, 0, 0);
  
  self.transitioningDelegate = [ModalTransitioningDelegate new];
  
  self.transactions = @[
                        @[
                          [self _transactionWithValue:@"-100"],
                          [self _transactionWithValue:@"-200"],
                          [self _transactionWithValue:@"-50"],
                          [self _transactionWithValue:@"+1500"],
                          ],
                        @[
                          [self _transactionWithValue:@"-15"],
                          [self _transactionWithValue:@"-150"],
                          [self _transactionWithValue:@"-20"],
                          ],
                        @[
                          [self _transactionWithValue:@"+500"],
                          [self _transactionWithValue:@"-20"],
                          [self _transactionWithValue:@"-240"],
                          [self _transactionWithValue:@"-5"],
                          [self _transactionWithValue:@"-7"],
                          ],
                        @[
                          [self _transactionWithValue:@"-10"],
                          [self _transactionWithValue:@"-40"],
                          ],
                        @[
                          [self _transactionWithValue:@"-100"],
                          [self _transactionWithValue:@"+800"],
                          ],
                        @[
                          [self _transactionWithValue:@"-25"],
                          [self _transactionWithValue:@"-140"],
                          ],
                        ];
  
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  
  self.firstLaunch = YES;
}

#pragma mark - Table view data source

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
  NSArray *dates = @[@"June 12", @"June 11", @"June 10",@"June 9", @"June 8", @"June 7"];
  return [TableHeaderView headerViewWithFrame:CGRectMake(0, 0, 320, 26) date:dates[section]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
  return 26;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return [self.transactions count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [self.transactions[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  TransactionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellID" forIndexPath:indexPath];

  Transaction *tr = self.transactions[indexPath.section][indexPath.row];
  [cell updateCellWithTransaction:tr];
  
  if ([tr.amount hasPrefix:@"+"]) {
    UIColor *color = [UIColor colorWithRed:38.0f/255.0f green:182.0f/255.0f blue:124.0f/255.0f alpha:1.0f];
    
    int tmp = (arc4random() % 30) + 1;

    NSString *name = tmp % 5 == 0 ? @"Salary" : @"Business" ;
    cell.transactionIcon.image = [[UIImage imageNamed:name] imageTintedWithColor:color];
    cell.transactionAmount.textColor = color;
    
    cell.transactionTitle.text = name;
  }
  
  if (indexPath.row == ([self.transactions[indexPath.section] count] -1)) {
    cell.separatorView.hidden = YES;
  }
  else {
    cell.separatorView.hidden = NO;
  }

  
  return cell;
}

#pragma mark — UIScrollView

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  return;
  
  if (!self.firstLaunch) {
    return;
  }
  
  CGFloat scrollOffset = scrollView.contentOffset.y;
  
  NSLog(@"%f", scrollOffset);
  
  [self.headerView animateForScrollOffset:scrollOffset];
  
  return;
  if (scrollOffset < -64){
    [UIView animateWithDuration:0.5
                     animations:^{
                       self.top.constant += -scrollOffset;
                       [self.view layoutIfNeeded];
                     }];

  }

}

#pragma mark — Private Methods

- (void)_showMenu {
  [self.frostedViewController presentMenuViewController];
}

- (void)_addNewTransaction {
  NavigationController *navVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewTransitionVC"];
  navVC.transitioningDelegate = self.transitioningDelegate;
  [self presentViewController:navVC animated:YES completion:nil];
}

- (Transaction *)_transactionWithValue:(NSString *)value {
  NSArray *categories = [TransactionCategory MR_findAll];
  Transaction *tr = [Transaction MR_createEntity];
  
  tr.category = categories[arc4random() % 16];
  tr.amount = value;
  
  return tr;
}

@end
