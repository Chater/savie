//
//  CustomLabel.m
//  Savie
//
//  Created by Chater on 7/6/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "CustomLabel.h"

@implementation CustomLabel

+ (instancetype)labelWithFrame:(CGRect)frame size:(CGFloat)size {
  CustomLabel *label = [[CustomLabel alloc] initWithFrame:frame];
  
  label.font = [UIFont fontWithName:kFontNameLight size:size];
  
  return label;
}

@end
