//
//  TableHeaderView.m
//  Savie
//
//  Created by Chater on 6/30/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "TableHeaderView.h"

@implementation TableHeaderView

+ (instancetype)headerViewWithFrame:(CGRect)frame date:(NSString *)dateString {
  TableHeaderView *view = [[TableHeaderView alloc] initWithFrame:frame];
  
  view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"table_header_background"]];
  
  UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0,
                                                                 view.frame.size.width - 30,
                                                                 view.frame.size.height)];
  dateLabel.font = [UIFont fontWithName:kFontNameRegular size:15];
  dateLabel.alpha = 0.8f;
  dateLabel.textColor = [UIColor whiteColor];
  dateLabel.text = dateString;
  
  [view addSubview:dateLabel];
  
  return view;
}

@end
