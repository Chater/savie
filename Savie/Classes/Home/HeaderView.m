//
//  HeaderView.m
//  Savie
//
//  Created by Chater on 6/29/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "HeaderView.h"
#import "CustomLabel.h"

@implementation HeaderView

- (instancetype)initWithCoder:(NSCoder *)coder {
  self = [super initWithCoder:coder];
  if (self) {
    self.clipsToBounds = YES;
    self.backgroundColor = [UIColor clearColor];
    
//    self.calendarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 150)];
////    self.calendarView.backgroundColor = [UIColor blueColor];
//    [self addSubview:self.calendarView];
    
    self.amountView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.amountView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"header_background"]];
    
    CustomLabel *availableBalance = [CustomLabel labelWithFrame:CGRectMake(20, 10, 280, 20) size:15];
    availableBalance.text = @"Available balance";
    availableBalance.textAlignment = NSTextAlignmentRight;
    availableBalance.textColor = [UIColor whiteColor];
    availableBalance.alpha = 0.8f;
    
    [self.amountView addSubview:availableBalance];
    
    
    CustomLabel *amount = [CustomLabel labelWithFrame:CGRectMake(20, 30, 280, 50) size:46];
    amount.text = @"$250,60";
    amount.textAlignment = NSTextAlignmentRight;
    amount.textColor = [UIColor whiteColor];
    
    [self.amountView addSubview:amount];
    
    [self addSubview:self.amountView];
    
  }
  return self;
}

- (void)animateForScrollOffset:(CGFloat)scrollOffset {
  if (scrollOffset > 0) {
    return;
  }
  
  NSLog(@"%f", scrollOffset);
  
//  self.frame = CGRectMake(0, 0, 320, self.frame.size.height + scrollOffset);
  self.amountView.frame = CGRectMake(0, scrollOffset, 320, 92);
}

@end
