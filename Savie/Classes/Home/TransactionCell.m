//
//  TransactionCell.m
//  Savie
//
//  Created by Chater on 25.06.14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "TransactionCell.h"

#import "UIImage+Tint.h"

@implementation TransactionCell

- (void)updateCellWithTransaction:(Transaction *)transaction {
  self.transactionTitle.text = transaction.category.name;

  self.transactionAmount.text = transaction.amount;

  UIColor *color = nil;
  if ([transaction.amount hasPrefix:@"-"]) {
    color = [UIColor colorWithRed:240.0f/255.0f green:90.0f/255.0f blue:78.0f/255.0f alpha:1.0f];
  }
  else {
    color = [UIColor colorWithRed:38.0f/255.0f green:182.0f/255.0f blue:124.0f/255.0f alpha:1.0f];
  }
  
  self.transactionIcon.image = [[UIImage imageNamed:transaction.category.imageName] imageTintedWithColor:color];
  self.transactionAmount.textColor = color;
}

- (void)awakeFromNib {
  self.backgroundColor = [UIColor clearColor];
  self.transactionTitle.font = [UIFont fontWithName:kFontNameLight size:18];
  self.transactionAmount.font = [UIFont fontWithName:kFontNameLight size:18];
}

@end
