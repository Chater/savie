//
//  ModalTransitioning.h
//  Savie
//
//  Created by Kostiantyn Myts on 7/2/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModalTransitioning : NSObject
<
UIViewControllerAnimatedTransitioning
>


@end
