//
//  TransactionDetailsTransitioning.m
//  Savie
//
//  Created by Chater on 7/6/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "TransactionDetailsTransitioning.h"
#import "CustomLabel.h"

#import "UIImage+Tint.h"

@interface TransactionDetailsTransitioning ()

@property (nonatomic, strong) Transaction *transaction;

@property (nonatomic, assign) CGRect imageFrame;
@property (nonatomic, assign) CGRect categoryNameFrame;
@property (nonatomic, assign) CGRect amountFrame;

@end

@implementation TransactionDetailsTransitioning

+ (instancetype)transitioningWithTransaction:(Transaction *)transaction
                                  imageFrame:(CGRect)imageFrame
                           categoryNameFrame:(CGRect)categoryNameFrame
                                 amountFrame:(CGRect)amountFrame {
  TransactionDetailsTransitioning *transitioning = [TransactionDetailsTransitioning new];
  
  transitioning.transaction = transaction;
  
  transitioning.imageFrame = imageFrame;
  transitioning.categoryNameFrame = categoryNameFrame;
  transitioning.amountFrame = amountFrame;
  
  return transitioning;
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
  return 1.0f;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
  UIView *container = transitionContext.containerView;
  
	UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
  UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
  
  UIView *fromView = fromVC.view;
  UIView *toView = toVC.view;
  
// view
  
  CGRect viewLeftBeginFrame = CGRectMake(-fromView.frame.size.width,
                                          fromView.frame.origin.y,
                                          fromView.frame.size.width,
                                          fromView.frame.size.height);
  
  CGRect viewRightBeginFrame = CGRectMake(fromView.frame.size.width,
                                         fromView.frame.origin.y,
                                         fromView.frame.size.width,
                                         fromView.frame.size.height);
  
  CGRect viewEndFrame = fromView.frame;
  
  if (self.push) {
    [container addSubview:toView];
    toView.frame = viewRightBeginFrame;
  }
  else {
    [container insertSubview:toView belowSubview:fromView];
    toView.frame = viewLeftBeginFrame;
  }
  
  UIImageView *imageView = [self _imageView];
  [container addSubview:imageView];
  
  CustomLabel *categoryNameLabel = [self _categoryNameLabel];
  [container addSubview:categoryNameLabel];
  
  CustomLabel *amountLabel = [self _amountLabel];
  [container addSubview:amountLabel];

  id animationBlock = ^{
    if (self.push) {
      fromView.frame = viewLeftBeginFrame;
      toView.frame = viewEndFrame;
  
      imageView.frame = [self _imageEndFrame];
      categoryNameLabel.frame = [self _categoryNameEndFrame];
//      amountLabel.frame = [self _amountEndFrame];
      amountLabel.frame = [self _amountEndFrame2:amountLabel];

    }
    else {
      fromView.frame = viewRightBeginFrame;
      toView.frame = viewEndFrame;
      
      imageView.frame = [self _imageBeginFrame];
      categoryNameLabel.frame = [self _categoryNameBeginFrame];
      amountLabel.frame = [self _amountBeginFrame];
    }
  };
  
  id completionBlock = ^{
    [imageView removeFromSuperview];
    [categoryNameLabel removeFromSuperview];
    [amountLabel removeFromSuperview];
    
    [fromView removeFromSuperview];
    
    [transitionContext completeTransition:YES];
  };
  
  [UIView animateWithDuration:1.0f
                   animations:animationBlock
                   completion:completionBlock];
  
}

#pragma mark — Getters 

- (UIImageView *)_imageView {
  UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.push ? [self _imageBeginFrame] : [self _imageEndFrame]];
  imageView.contentMode = UIViewContentModeScaleAspectFit;
  
  UIColor *imageColor = self.push ? [UIColor whiteColor] : [UIColor selectedCategoryColor];
  imageColor = [UIColor selectedCategoryColor];
  
  imageView.image = [[UIImage imageNamed:self.transaction.category.imageName] imageTintedWithColor:imageColor];
  
  return imageView;
}

- (CGRect)_imageBeginFrame {
  return CGRectMake(CGRectGetMinX(self.imageFrame),
                    CGRectGetMinY(self.imageFrame) + 64,
                    CGRectGetWidth(self.imageFrame),
                    CGRectGetHeight(self.imageFrame));
}

- (CGRect)_imageEndFrame {
  return CGRectMake(10, 9 + 64, 35, 35);
}

///

- (CustomLabel *)_categoryNameLabel {
  CGRect frame = self.push ? [self _categoryNameBeginFrame] : [self _categoryNameEndFrame];
  CustomLabel *label = [CustomLabel labelWithFrame:frame
                                              size:20];
  label.textColor = [UIColor selectedCategoryColor];
  label.text = self.transaction.category.name;
//  label.frame = [self _categoryNameEndFrame2:label];
//  
//  if (self.push) {
//    label.center = CGPointMake(CGRectGetMinX(frame) + frame.size.width / 2,
//                               CGRectGetMinY(frame) + frame.size.height / 2);
//  }
  
  return label;
}

- (CGRect)_categoryNameBeginFrame {
  //20 is magic number
  return CGRectMake(CGRectGetMinX(self.categoryNameFrame) + 20,
                    CGRectGetMinY(self.categoryNameFrame) + 64,
                    CGRectGetWidth(self.categoryNameFrame),
                    CGRectGetHeight(self.categoryNameFrame));
}

- (CGRect)_categoryNameEndFrame {
  return CGRectMake(55, 64, 120, 53);
}

- (CGRect)_categoryNameEndFrame2:(CustomLabel *)label {
  return CGRectMake(55, 64, label.frame.size.width, 53);
}

///

- (CustomLabel *)_amountLabel {
  CustomLabel *label = [CustomLabel labelWithFrame:self.push ? [self _amountBeginFrame] : [self _amountEndFrame]
                                              size:25];
  
  label.textColor = [UIColor whiteColor];
  label.text = self.transaction.amount;
  
  if (!self.push) {
    [label sizeToFit];
    label.frame = [self _amountEndFrame2:label];
  }
  
    NSLog(@"push %hhd — %@", self.push ,NSStringFromCGRect(label.frame));


  return label;
}

- (CGRect)_amountBeginFrame {
  return CGRectMake(CGRectGetMinX(self.amountFrame),
                    CGRectGetMinY(self.amountFrame) + 64,
                    CGRectGetWidth(self.amountFrame),
                    CGRectGetHeight(self.amountFrame));
}

- (CGRect)_amountEndFrame {
  return CGRectMake(200, 64, 100, 53);
}

- (CGRect)_amountEndFrame2:(CustomLabel *)label {
  return CGRectMake(300 - CGRectGetWidth(label.frame),
                    64,
                    CGRectGetWidth(label.frame),
                    53);
}

@end
