//
//  ModalTransitioning.m
//  Savie
//
//  Created by Kostiantyn Myts on 7/2/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "ModalTransitioning.h"

@implementation ModalTransitioning

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
  return 1.0f;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
  UIView *container = transitionContext.containerView;
	UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
  UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
  UIView *fromView = fromVC.view;
  UIView *toView = toVC.view;
  
  CGRect beginFrame = CGRectMake(0,
                                 fromView.frame.size.height,
                                 fromView.frame.size.width,
                                 fromView.frame.size.height);
  
  CGRect endFrame = fromView.frame;
  
  if (toVC.isBeingPresented) {
    [container addSubview:toView];
    toView.frame = beginFrame;
  }
  else {
    [container insertSubview:toView belowSubview:fromView];
    toView.frame = endFrame;
    toView.alpha = 0.0f;
    toView.transform = CGAffineTransformMakeScale(0.5f, 0.5f);
  }
  
  id animationBlock = ^{
    if (toVC.isBeingPresented) {
      toView.frame = endFrame;
      fromView.alpha = 0.0f;
      fromView.transform = CGAffineTransformMakeScale(0.5f, 0.5f);
    }
    else {
      toView.alpha = 1.0f;
      toView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
      fromView.frame = beginFrame;
    }
  };
  
  id completionBlock = ^{
    if (toVC.isBeingPresented) {
      fromView.frame = endFrame;
    }
    
    [fromView removeFromSuperview];
    
    NSLog(@"done with animation");
    [transitionContext completeTransition:YES];
  };
  
  [UIView animateWithDuration:0.99f
                        delay:0
       usingSpringWithDamping:0.8f
        initialSpringVelocity:0.5f
                      options:UIViewAnimationOptionCurveEaseOut
                   animations:animationBlock
                   completion:completionBlock];
}


@end
