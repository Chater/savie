//
//  TransactionDetailsTransitioning.h
//  Savie
//
//  Created by Chater on 7/6/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransactionDetailsTransitioning : NSObject
<
UIViewControllerAnimatedTransitioning
>

@property (nonatomic, assign) BOOL push;

+ (instancetype)transitioningWithTransaction:(Transaction *)transaction
                                  imageFrame:(CGRect)imageFrame
                           categoryNameFrame:(CGRect)categoryNameFrame
                                 amountFrame:(CGRect)amountFrame;

@end
