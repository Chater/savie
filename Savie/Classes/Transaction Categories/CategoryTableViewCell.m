//
//  CategoryTableViewCell.m
//  Savie
//
//  Created by Chater on 7/11/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "CategoryTableViewCell.h"

@implementation CategoryTableViewCell

- (void)awakeFromNib {
  self.backgroundColor = [UIColor clearColor];
  self.categoryTitle.font = [UIFont fontWithName:kFontNameLight size:18];
}


@end
