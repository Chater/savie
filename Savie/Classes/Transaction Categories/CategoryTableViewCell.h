//
//  CategoryTableViewCell.h
//  Savie
//
//  Created by Chater on 7/11/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *categoryImageView;
@property (nonatomic, weak) IBOutlet UILabel *categoryTitle;
@property (nonatomic, weak) IBOutlet UIImageView *separatorView;

@end
