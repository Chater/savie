//
//  CategoriesViewController.m
//  Savie
//
//  Created by Chater on 7/11/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "CategoriesViewController.h"
#import "CategoryTableViewCell.h"
#import "REFrostedViewController.h"

#import "UIImage+Tint.h"

@interface CategoriesViewController ()

@property (nonatomic, strong) NSMutableArray *categories;

@end

@implementation CategoriesViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.categories = [NSMutableArray arrayWithArray:[TransactionCategory MR_findAllSortedBy:@"createdDate" ascending:YES]];
  
  [self.tableView setEditing:YES animated:YES];
  
  self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"slide_menu_icon"]
                                                                           style:UIBarButtonItemStyleBordered
                                                                          target:self
                                                                          action:@selector(_showMenu)];
  
  self.title = @"Categories";
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [self.categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  CategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellID" forIndexPath:indexPath];
  
  
  UIColor *color = [UIColor colorWithRed:240.0f/255.0f green:90.0f/255.0f blue:78.0f/255.0f alpha:1.0f];

  TransactionCategory *category = self.categories[indexPath.row];
  
  cell.categoryImageView.image = [[UIImage imageNamed:category.imageName] imageTintedWithColor:color];
  
  cell.categoryTitle.textColor = [UIColor colorWithRed:0.6392 green:0.6431 blue:0.6549 alpha:1.0];
  
  cell.categoryTitle.text = category.name;
  
  cell.showsReorderControl = YES;
  
  if (indexPath.row == ([self.categories count] -1)) {
    cell.separatorView.hidden = YES;
  }
  else {
    cell.separatorView.hidden = NO;
  }
  
  return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
  return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
  if (editingStyle == UITableViewCellEditingStyleDelete) {
    [self.categories removeObjectAtIndex:indexPath.row];
    
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];

  }
}

- (void)_showMenu {
  [self.frostedViewController presentMenuViewController];
}

@end
