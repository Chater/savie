//
//  UIColor+CustomColors.h
//  Savie
//
//  Created by Chater on 7/2/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColors)

+ (instancetype)categoryColor;
+ (instancetype)selectedCategoryColor;

@end
