//
//  UIColor+CustomColors.m
//  Savie
//
//  Created by Chater on 7/2/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "UIColor+CustomColors.h"

@implementation UIColor (CustomColors)

+ (instancetype)categoryColor {
  return [UIColor colorWithRed:0.3490 green:0.3490 blue:0.3490 alpha:1.0];
}

+ (instancetype)selectedCategoryColor {
  return [UIColor colorWithRed:240/255.0f
                         green:90/255.0f
                          blue:78/255.0f
                         alpha:1.0f];
}

@end
