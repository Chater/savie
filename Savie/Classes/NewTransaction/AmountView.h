//
//  AmountView.h
//  Savie
//
//  Created by Chater on 7/2/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AmountView : UIView

@property (nonatomic, weak) IBOutlet UIImageView *categoryImage;
@property (nonatomic, weak) IBOutlet UILabel *categoryLabel;
@property (nonatomic, weak) IBOutlet UILabel *amountLabel;

- (void)hideEverything:(BOOL)hide;
- (void)updateViewWithTransaction:(Transaction *)transaction;

- (void)setAmount:(NSString *)amount;

@end
