//
//  AmountView.m
//  Savie
//
//  Created by Chater on 7/2/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "AmountView.h"

@implementation AmountView

- (void)awakeFromNib {
  self.categoryImage.contentMode = UIViewContentModeScaleAspectFit;
  
  self.categoryLabel.font = [UIFont fontWithName:kFontNameLight size:25];
  self.categoryLabel.textColor = [UIColor whiteColor];
  
  self.amountLabel.font = [UIFont fontWithName:kFontNameLight size:25];
  self.amountLabel.textColor = [UIColor whiteColor];
  self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"amount_view"]];
}

- (void)hideEverything:(BOOL)hide {
  for (UIView *view in self.subviews) {
    view.hidden = hide;
  }
}

- (void)updateViewWithTransaction:(Transaction *)transaction {
  //need to remove this
  self.categoryLabel.font = [UIFont fontWithName:kFontNameLight size:20];
  self.amountLabel.font = [UIFont fontWithName:kFontNameLight size:25];
  
  self.categoryImage.image = [UIImage imageNamed:transaction.category.imageName];
  
  self.categoryLabel.text = transaction.category.name;
  
//  self.amountLabel.text = transaction.amount;
  [self setAmount:transaction.amount];
}

- (void)setAmount:(NSString *)amount {
  self.amountLabel.text = amount;
  [self.amountLabel sizeToFit];
  
  self.amountLabel.frame = CGRectMake(CGRectGetWidth(self.frame) - 20 - CGRectGetWidth(self.amountLabel.frame),
                                                 0,
                                                 CGRectGetWidth(self.amountLabel.frame),
                                                 53);
  NSLog(@"end frame %@", NSStringFromCGRect(self.amountLabel.frame));
  
}

@end
