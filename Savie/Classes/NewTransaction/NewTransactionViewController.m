//
//  NewTransactionViewController.m
//  Savie
//
//  Created by Chater on 6/30/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "NewTransactionViewController.h"
#import "CategoryCell.h"
#import "AmountView.h"
#import "CalculatorView.h"
#import "TransactionDetailsTransitioning.h"
#import "NewTransactionDetailsViewController.h"

#import "UIImage+Tint.h"

@interface NewTransactionViewController ()
<
UICollectionViewDelegate,
UICollectionViewDataSource,
UINavigationControllerDelegate,
CalculatorDelegate
>

@property (nonatomic, strong) NSArray *categories;

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UIPageControl *pageControl;

@property (nonatomic, weak) IBOutlet AmountView *amountView;
@property (nonatomic, weak) IBOutlet CalculatorView *calculatorView;

@property (nonatomic, strong) TransactionDetailsTransitioning *detailsTransitioning;

@end

@implementation NewTransactionViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.navigationController.delegate = self;
  
  self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
  
  self.calculatorView.delegate = self;
  
  self.amountView.amountLabel.text = @"0";
  
  self.pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:0.1608 green:0.6353 blue:0.6235 alpha:1.0];
  self.pageControl.pageIndicatorTintColor = [UIColor colorWithRed:0.6863 green:0.8275 blue:0.8275 alpha:1.0];
  self.pageControl.alpha = 0.0f;
  
  self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close_icon"]
                                                                           style:UIBarButtonItemStyleBordered
                                                                          target:self
                                                                          action:@selector(_dismiss)];
  
  self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"save_icon"]
                                                                           style:UIBarButtonItemStyleBordered
                                                                          target:self
                                                                          action:@selector(_showDetailsView)];
  self.navigationItem.rightBarButtonItem.enabled = NO;
  
  self.categories = [TransactionCategory MR_findAllSortedBy:@"createdDate" ascending:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  NSIndexPath *selectedIndexPath = [self.collectionView.indexPathsForSelectedItems firstObject];
  CategoryCell *cell = (CategoryCell *)[self.collectionView cellForItemAtIndexPath:selectedIndexPath];
  
  cell.categoryIcon.alpha = 0.0f;
  cell.categoryName.alpha = 0.0f;
  
  self.amountView.amountLabel.alpha = 0.0f;
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  
  NSIndexPath *selectedIndexPath = [self.collectionView.indexPathsForSelectedItems firstObject];
  CategoryCell *cell = (CategoryCell *)[self.collectionView cellForItemAtIndexPath:selectedIndexPath];
  
  cell.categoryIcon.alpha = 1.0f;
  cell.categoryName.alpha = 1.0f;
  
  self.amountView.amountLabel.alpha = 1.0f;
}

#pragma mark — UICollectionViewDelegate Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return [self.categories count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  CategoryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CategoryCellID"
                                                                 forIndexPath:indexPath];
  
  NSIndexPath *selectedIndexPath = [collectionView.indexPathsForSelectedItems firstObject];
  
  UIColor *color = nil;
  CGAffineTransform transform;
  
  if (selectedIndexPath && [selectedIndexPath compare:indexPath] == NSOrderedSame) {
    color = [UIColor selectedCategoryColor];
    transform = CGAffineTransformMakeScale(1.3f, 1.3f);
  }
  else {
    color = [UIColor categoryColor];
    transform = CGAffineTransformMakeScale(1.0f, 1.0f);
  }
  
  TransactionCategory *category = self.categories[indexPath.row];
  
  cell.categoryIcon.image = [[UIImage imageNamed:category.imageName] imageTintedWithColor:color];
  
  cell.categoryName.text = category.name;
  cell.categoryName.textColor = color;
  
  cell.categoryIcon.transform = transform;
  cell.categoryName.transform = transform;
  
  [self _updatePageControl];
  
  return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
  self.navigationItem.rightBarButtonItem.enabled = YES;
  
  CategoryCell *cell = (CategoryCell *)[collectionView cellForItemAtIndexPath:indexPath];
  
  POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
  scaleAnimation.springBounciness = 15;
  scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.3f, 1.3f)];
  
  [cell.categoryIcon pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
  [cell.categoryName pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
  
  [UIView animateWithDuration:0.5f animations:^{
    cell.categoryName.textColor = [UIColor selectedCategoryColor];
    cell.categoryIcon.image = [cell.categoryIcon.image imageTintedWithColor:[UIColor selectedCategoryColor]];
  }];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
  CategoryCell *cell = (CategoryCell *)[collectionView cellForItemAtIndexPath:indexPath];
  
  POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
  scaleAnimation.springBounciness = 10;
  scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1.0f, 1.0f)];
  
  [cell.categoryIcon pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
  [cell.categoryName pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
  
  [UIView animateWithDuration:0.5f animations:^{
    cell.categoryName.textColor = [UIColor categoryColor];
    cell.categoryIcon.image = [cell.categoryIcon.image imageTintedWithColor:[UIColor categoryColor]];
  }];
}

#pragma mark — UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  self.pageControl.currentPage = scrollView.contentOffset.x / scrollView.frame.size.width + 1;
}

- (void)calculatorView:(CalculatorView *)calculatorView didPressNumber:(NSInteger)number {
  NSString *oldText = self.amountView.amountLabel.text;
  
  NSString *newNumber = [NSString stringWithFormat:@"%i", number];

  if ([oldText isEqualToString:@"0"]) {
    [self.amountView setAmount:newNumber];
    return;
  }
  
  [self.amountView setAmount:[self.amountView.amountLabel.text stringByAppendingString:newNumber]];
}

#pragma mark —

- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                   animationControllerForOperation:(UINavigationControllerOperation)operation
                                                fromViewController:(UIViewController *)fromVC
                                                  toViewController:(UIViewController *)toVC {
  if (self.detailsTransitioning) {
    self.detailsTransitioning.push = operation == UINavigationControllerOperationPush;
    
    return self.detailsTransitioning;
  }
  
  return nil;
}

#pragma mark — Private Methods

- (void)_dismiss {
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)_showDetailsView {
  NSIndexPath *selectedIndexPath = [self.collectionView.indexPathsForSelectedItems firstObject];
  CategoryCell *cell = (CategoryCell *)[self.collectionView cellForItemAtIndexPath:selectedIndexPath];
  
  [self.amountView setAmount:self.amountView.amountLabel.text];
  
  CGRect imageSuperFrame = [cell convertRect:cell.categoryIcon.frame
                                     toView:self.collectionView.superview];
  
  CGRect categoryNameSuperFrame = [cell convertRect:cell.categoryName.frame
                                             toView:self.collectionView.superview];
  
  CGRect amountSuperFrame = [self.amountView convertRect:self.amountView.amountLabel.frame
                                            toView:self.view];
  
  Transaction *transition = [Transaction MR_createEntity];
  transition.amount = self.amountView.amountLabel.text;
  transition.category = self.categories[selectedIndexPath.row];
  
  self.detailsTransitioning = [TransactionDetailsTransitioning transitioningWithTransaction:transition
                                                                                 imageFrame:imageSuperFrame
                                                                          categoryNameFrame:categoryNameSuperFrame
                                                                                amountFrame:amountSuperFrame];
  
  NewTransactionDetailsViewController *newTransactionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewTransactionDetailsVC"];
  
  newTransactionVC.transaction = transition;
  
  [self.navigationController pushViewController:newTransactionVC animated:YES];
}

- (void)_updatePageControl {
  self.pageControl.alpha = 1.0f;
  self.pageControl.numberOfPages = self.collectionView.contentSize.width / self.collectionView.frame.size.width + 1;
}

@end
