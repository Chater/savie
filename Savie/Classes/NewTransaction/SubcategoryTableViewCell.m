//
//  SubcategoryTableViewCell.m
//  Savie
//
//  Created by Chater on 7/10/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "SubcategoryTableViewCell.h"

@implementation SubcategoryTableViewCell

- (void)awakeFromNib {
  self.subcategoryName.font = [UIFont fontWithName:kFontNameLight size:20];
  self.subcategoryName.textColor = [UIColor whiteColor];
}

@end
