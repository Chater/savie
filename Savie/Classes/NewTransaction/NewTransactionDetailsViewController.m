//
//  NewTransactionDetailsViewController.m
//  Savie
//
//  Created by Chater on 7/6/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "NewTransactionDetailsViewController.h"
#import "AmountView.h"
#import "SubcategoryTableViewCell.h"

#import "UIImage+Tint.h"

@interface NewTransactionDetailsViewController ()

@property (nonatomic, weak) IBOutlet AmountView *amountView;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@end

@implementation NewTransactionDetailsViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  [self.amountView hideEverything:YES];
  
  [self.amountView updateViewWithTransaction:self.transaction];
  
  self.amountView.categoryLabel.textColor = [UIColor selectedCategoryColor];
  
  self.view.backgroundColor = [UIColor colorWithRed:0.2980 green:0.7490 blue:0.7451 alpha:1.0];
  
  self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_icon"]
                                                                           style:UIBarButtonItemStyleBordered
                                                                          target:self
                                                                          action:@selector(_popVC)];
  
  self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"save_icon"]
                                                                            style:UIBarButtonItemStyleBordered
                                                                           target:self
                                                                           action:@selector(_dismiss)];
  
  self.tableView.backgroundColor = [UIColor clearColor];
  self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  
  [self.amountView hideEverything:NO];
  [self.amountView updateViewWithTransaction:self.transaction];
  
  POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPLabelTextColor];
  anim.toValue = [UIColor whiteColor];

  [self.amountView.categoryLabel pop_addAnimation:anim forKey:@"anim"];
  
//  
//  [UIView transitionWithView:self.amountView.categoryImage
//                    duration:0.8f
//                     options:UIViewAnimationOptionTransitionCrossDissolve
//                  animations:^{
  self.amountView.categoryImage.image = [[UIImage imageNamed:self.transaction.category.imageName] imageTintedWithColor:[UIColor whiteColor]];
//                  } completion:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  
  [self.amountView hideEverything:YES];
}

#pragma mark 

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  SubcategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellID" forIndexPath:indexPath];
  
  NSArray *sub = @[@"Books", @"Supplies", @"Tuition",@"+"];
  cell.subcategoryName.text = sub[indexPath.row];
  
  return cell;
}


#pragma mark — Private Methods

- (void)_popVC {
  [self.navigationController popViewControllerAnimated:YES];
}

- (void)_dismiss {
  [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

@end
