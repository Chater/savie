//
//  CalculatorView.m
//  Savie
//
//  Created by Kostiantyn Myts on 7/2/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "CalculatorView.h"

@interface CalculatorView ()

@property (nonatomic, assign) NSString *valuesString;

@end

@implementation CalculatorView

- (void)awakeFromNib {
  for (UIButton *button in self.subviews) {
    button.backgroundColor = [UIColor clearColor];
    button.titleLabel.font = [UIFont fontWithName:kFontNameLight size:25];
    button.layer.cornerRadius = button.frame.size.height / 2;
    
    [button addTarget:self
               action:@selector(didPressButton:)
     forControlEvents:UIControlEventTouchUpInside];
    
    if ([button.titleLabel.text length] == 1) {
      button.tag = [button.titleLabel.text integerValue];
    } else {
      button.tag = -1;
    }
  }
  
  self.backgroundColor = [UIColor colorWithRed:0.0f
                                         green:159.0f/255.0f
                                          blue:159.0f/255.0f
                                         alpha:0.7f];
}

- (void)didPressButton:(UIButton *)button {
  NSLog(@"%i", button.tag);
  
  [UIView animateWithDuration:0.2f animations:^{
    button.backgroundColor = [UIColor colorWithRed:0.1843 green:0.6275 blue:0.6235 alpha:1.0];
  } completion:^(BOOL success){
    [UIView animateWithDuration:0.2f animations:^{
      button.backgroundColor = [UIColor clearColor];
    }];
  }];
  
  [self.delegate calculatorView:self didPressNumber:button.tag];
  
//  POPBasicAnimation *anim = [POPBasicAnimation animationWithPropertyNamed:kPOPViewBackgroundColor];
//  anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//  anim.fromValue = button.backgroundColor;
//  anim.toValue = [UIColor blackColor];
//  
//  [button pop_addAnimation:anim forKey:@"fade"];
}

@end
