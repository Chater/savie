//
//  CategoryCell.m
//  Savie
//
//  Created by Kostiantyn Myts on 7/2/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "CategoryCell.h"

@implementation CategoryCell

- (void)awakeFromNib {
  self.categoryName.font = [UIFont fontWithName:kFontNameLight size:15];
}

@end
