//
//  CategoryCell.h
//  Savie
//
//  Created by Kostiantyn Myts on 7/2/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *categoryIcon;
@property (nonatomic, weak) IBOutlet UILabel *categoryName;

@end
