//
//  SubcategoryTableViewCell.h
//  Savie
//
//  Created by Chater on 7/10/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubcategoryTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *subcategoryName;

@end
