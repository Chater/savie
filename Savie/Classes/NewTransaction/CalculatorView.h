//
//  CalculatorView.h
//  Savie
//
//  Created by Kostiantyn Myts on 7/2/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
  CalculatorButtonAdd,
  CalculatorButtonSubstract,
  CalculatorButtonEqual,
} CalculatorButtonType;

@class CalculatorView;
@protocol CalculatorDelegate <NSObject>

- (void)calculatorView:(CalculatorView *)calculatorView didPressNumber:(NSInteger)number;
//- (void)calculatorView:(CalculatorView *)calculatorView didPressButton:(CalculatorButtonType)buttonType;

@end

@interface CalculatorView : UIView

@property (nonatomic, assign) id<CalculatorDelegate>delegate;

@end
