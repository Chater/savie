//
//  Constants.m
//  Savie
//
//  Created by Chater on 6/30/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "Constants.h"

NSString * const kFontNameRegular = @"Roboto-Regular";
NSString * const kFontNameLight = @"Roboto-Light";

@implementation Constants

@end
