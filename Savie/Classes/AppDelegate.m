//
//  AppDelegate.m
//  Savie
//
//  Created by Chater on 24.06.14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "AppDelegate.h"
#import "COSTouchVisualizerWindow.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  
  [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"nav_bar_background"] forBarMetrics:UIBarMetricsDefault];
  [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
  
  [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
  
  [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
  
  [MagicalRecord setupCoreDataStack];
  
  NSArray *categories = [TransactionCategory MR_findAll];
  NSArray *categoryProperties = @[
                                  @[@"Food", @"food"],
                                  @[@"Studies", @"studies"],
                                  @[@"Car", @"car"],
                                  @[@"Home", @"home"],
                                  @[@"Personal", @"personal"],
                                  @[@"Travel", @"travel"],
                                  @[@"Utilities", @"utilities"],
                                  @[@"Clothing", @"clothing"],
                                  @[@"Health", @"health"],
                                  @[@"Transport", @"transport"],
                                  @[@"Repair", @"repair"],
                                  @[@"Alcohol", @"alcohol"],
                                  @[@"Kids", @"kids"],
                                  @[@"Sport", @"sport"],
                                  @[@"Hobby", @"hobby"],
                                  @[@"Gifts", @"gift"],
                                  @[@"Pets", @"pets"],
                                  @[@"Drink", @"drink"],
                                  @[@"Savie", @"pig"],
                                  ];
  if ([categories count] == 0) {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
      for (NSArray *categoryPropertie in categoryProperties) {
        TransactionCategory *category = [TransactionCategory MR_createEntityInContext:localContext];
        category.name = categoryPropertie[0];
        category.imageName = categoryPropertie[1];
        category.createdDate = [NSDate date];
      }
      
    }
     completion:^(BOOL success, NSError *error) {
       NSLog(@"/nsucces: %hhd /nerror: %@", success, error);
     
     }];
  }
  
  
  return YES;
}

- (COSTouchVisualizerWindow *)window
{
  static COSTouchVisualizerWindow *customWindow = nil;
  if (!customWindow) {
    customWindow = [[COSTouchVisualizerWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [customWindow setFillColor:[UIColor whiteColor]];
    [customWindow setStrokeColor:[UIColor colorWithRed:0.2 green:0.7 blue:0.7 alpha:1.0]];
    [customWindow setTouchAlpha:0.4];
    
    [customWindow setRippleFillColor:[UIColor whiteColor]];
    [customWindow setRippleStrokeColor:[UIColor colorWithRed:0.1 green:0.6 blue:0.6 alpha:1.0]];
    [customWindow setRippleAlpha:0.5];
  }
  return customWindow;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
  // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
  // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
  // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
  // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
  // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
  // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
  // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
