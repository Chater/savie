//
//  Constants.h
//  Savie
//
//  Created by Chater on 6/30/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kFontNameRegular;
extern NSString * const kFontNameLight;

@interface Constants : NSObject

@end
