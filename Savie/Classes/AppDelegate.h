//
//  AppDelegate.h
//  Savie
//
//  Created by Chater on 24.06.14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
