//
//  CCMenuCell.h
//  Savie
//
//  Created by Chater on 24.06.14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *menuIcon;
@property (nonatomic, weak) IBOutlet UILabel *menuTitle;

@end
