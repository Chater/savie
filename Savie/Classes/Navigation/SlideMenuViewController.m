//
//  CCMenuViewController.m
//  Savie
//
//  Created by Chater on 24.06.14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "SlideMenuViewController.h"
#import "REFrostedViewController.h"
#import "MenuCell.h"

@interface SlideMenuViewController ()

@end

@implementation SlideMenuViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.tableView.backgroundColor = [UIColor clearColor];
  self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
  self.tableView.scrollEnabled = NO;
  self.tableView.contentInset = UIEdgeInsetsMake(60, 0, 0, 0);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCellID" forIndexPath:indexPath];
  
  cell.backgroundColor = [UIColor clearColor];
  
  NSArray *iconImageNames = @[@"home_icon", @"accounts_icon", @"categories_icon", @"charts_icon", @"settings_icon"];
  NSArray *titles = @[@"Home", @"Accounts", @"Categories", @"Charts", @"Settings"];
  
  cell.menuIcon.image = [UIImage imageNamed:iconImageNames[indexPath.row]];
  cell.menuIcon.alpha = 0.7f;
  
  cell.menuTitle.text = titles[indexPath.row];
  cell.menuTitle.textColor = [UIColor whiteColor];
  cell.menuTitle.alpha = 0.7f;
  
  if (indexPath.row != 0) {
    UIImageView *separator = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"slide_separator"]];
    separator.alpha = 0.5f;
//    separator.frame = CGRectMake(0,
//                                 CGRectGetMaxY(cell.frame) - 10,
//                                 CGRectGetWidth(separator.frame),
//                                 CGRectGetHeight(separator.frame));
    
    [cell addSubview:separator];
  }
  
  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  if (indexPath.row == 0) {
    self.frostedViewController.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MainVC"];
  }
  
  if (indexPath.row == 1) {
    self.frostedViewController.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AccountsVC"];
  }
  
  if (indexPath.row == 2) {
    self.frostedViewController.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoriesVC"];
  }

  [self.frostedViewController hideMenuViewController];
}

@end
