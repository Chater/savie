//
//  CCMenuCell.m
//  Savie
//
//  Created by Chater on 24.06.14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

- (void)awakeFromNib {
  self.menuTitle.font = [UIFont fontWithName:kFontNameRegular size:18];
}

@end
