//
//  CCNavigationController.m
//  Savie
//
//  Created by Chater on 24.06.14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "NavigationController.h"
#import "REFrostedViewController.h"

@interface NavigationController ()

@end

@implementation NavigationController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.navigationBar.tintColor = [UIColor whiteColor];
//  self.navigationBar.translucent = YES;
  
  [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                         NSForegroundColorAttributeName : [UIColor whiteColor],
                                                         NSFontAttributeName : [UIFont fontWithName:kFontNameRegular size:20]
                                                         }];
  
  self.frostedViewController.limitMenuViewSize = YES;
  self.frostedViewController.menuViewSize = CGSizeMake(200, self.view.frame.size.height);
  self.frostedViewController.liveBlurBackgroundStyle = REFrostedViewControllerLiveBackgroundStyleDark;
  
  [self.view addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(panGestureRecognized:)]];
  
}

- (void)_showMenu {
  [self.frostedViewController.view endEditing:YES];
  [self.frostedViewController presentMenuViewController];
}

- (void)panGestureRecognized:(UIPanGestureRecognizer *)sender {
  [self.view endEditing:YES];
  [self.frostedViewController.view endEditing:YES];
  
  [self.frostedViewController panGestureRecognized:sender];
}

@end
