//
//  CCRootViewController.m
//  Savie
//
//  Created by Chater on 24.06.14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)awakeFromNib {
  self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MainVC"];
  self.menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuVC"];
}

@end
