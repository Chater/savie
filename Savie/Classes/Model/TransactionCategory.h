//
//  TransactionCategory.h
//  Savie
//
//  Created by Chater on 7/10/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Transaction;

@interface TransactionCategory : NSManagedObject

@property (nonatomic, retain) NSString * imageName;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDate * createdDate;
@property (nonatomic, retain) NSSet *transactions;
@end

@interface TransactionCategory (CoreDataGeneratedAccessors)

- (void)addTransactionsObject:(Transaction *)value;
- (void)removeTransactionsObject:(Transaction *)value;
- (void)addTransactions:(NSSet *)values;
- (void)removeTransactions:(NSSet *)values;

@end
