//
//  TransactionCategory.m
//  Savie
//
//  Created by Chater on 7/10/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "TransactionCategory.h"
#import "Transaction.h"


@implementation TransactionCategory

@dynamic imageName;
@dynamic name;
@dynamic createdDate;
@dynamic transactions;

@end
