//
//  Transaction.m
//  Savie
//
//  Created by Chater on 7/6/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "Transaction.h"
#import "TransactionCategory.h"


@implementation Transaction

@dynamic amount;
@dynamic comment;
@dynamic category;

@end
