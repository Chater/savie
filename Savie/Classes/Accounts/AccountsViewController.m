//
//  AccountsViewController.m
//  Savie
//
//  Created by Chater on 7/11/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "AccountsViewController.h"
#import "AccountCell.h"
#import "REFrostedViewController.h"

#import "UIImage+Tint.h"

@interface AccountsViewController ()

@property (nonatomic, strong) NSArray *accounts;

@end

@implementation AccountsViewController
- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"slide_menu_icon"]
                                                                           style:UIBarButtonItemStyleBordered
                                                                          target:self
                                                                          action:@selector(_showMenu)];
  
  self.accounts = @[@[@"Cash", @"1450"],
                    @[@"PrivatBank", @"$1200"],
                    @[@"Aval", @"$125"],
                    @[@"UkrSibBank", @"$300"]
                    ];
  
  self.title   = @"Accounts";
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [self.accounts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  AccountCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellID" forIndexPath:indexPath];
  
  cell.accountTitle.text = self.accounts[indexPath.row][0];
  cell.accountAmount.text = self.accounts[indexPath.row][1];
  
  if (indexPath.row == ([self.accounts count] - 1)) {
    cell.separatorView.hidden = YES;
  }
  else {
    cell.separatorView.hidden = NO;
  }
  
  return cell;
}

- (void)_showMenu {
  [self.frostedViewController presentMenuViewController];
}
@end
