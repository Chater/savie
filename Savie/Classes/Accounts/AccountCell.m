//
//  AccountCell.m
//  Savie
//
//  Created by Chater on 7/11/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import "AccountCell.h"

@implementation AccountCell

- (void)awakeFromNib {
  self.accountAmount.textColor = [UIColor colorWithRed:0.3804 green:0.3804 blue:0.3843 alpha:1.0];
  self.accountTitle.textColor = [UIColor colorWithRed:0.3804 green:0.3804 blue:0.3843 alpha:1.0];
  self.accountTitle.font = [UIFont fontWithName:kFontNameLight size:18];
  self.accountAmount.font = [UIFont fontWithName:kFontNameLight size:18];
}


@end
