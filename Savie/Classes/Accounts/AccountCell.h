//
//  AccountCell.h
//  Savie
//
//  Created by Chater on 7/11/14.
//  Copyright (c) 2014 Constantine Chater. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *accountTitle;
@property (nonatomic, weak) IBOutlet UILabel *accountAmount;
@property (nonatomic, weak) IBOutlet UIImageView *separatorView;

@end
